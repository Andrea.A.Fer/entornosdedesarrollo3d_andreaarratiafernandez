using System;
using UnityEngine;


[RequireComponent(typeof(CharacterController))]
[RequireComponent (typeof(Animator))]
public class CharacterAnimBasedMovement : MonoBehaviour
{
    public float rotationSpeed = 4f;

    public float rotationThreshold = 0.3f;

    public int degreesToTurn = 160;

    [Header("Animator Parameters")]

    public string motionParam = "motion";

    public string mirrorIdleParam = "mirrorIdle";

    public string turn180Param = "turn180";

    [Header("Animation Smoothing")]
    [Range(0, 1f)]

    public float StartAnimTime = 0.3f;
    [Range(0, 1f)]
    public float StopAnimTime = 0.15f;

    [Header("Jump Settings")]
    public float jumpHeight = 2.0f; // Altura del salto
    public float gravityValue = -9.81f; // Valor de la gravedad
    private bool isGrounded; // Para verificar si el personaje est� en el suelo
    private Vector3 playerVelocity; // Velocidad del jugador



    private float Speed;

    private Vector3 desiredMoveDirection;

    private CharacterController characterController;

    private Animator animator;

    private bool mirrorIdle;

    private bool turns180;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        // Verificar si el personaje est� en el suelo
        isGrounded = characterController.isGrounded;
        if (isGrounded && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
            animator.SetBool("isJumping", false); // Aseg�rae de que el personaje no est� marcado como saltando si est� en el suelo.
        }

        // Detectar entrada para ataque
        if (Input.GetKeyDown(KeyCode.R))
        {
            Attack();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            Defend();
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
           Jump();
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            Salute();
        }
    }

    void Salute()
    {
        animator.SetTrigger("Salute");
    }

    void Jump()
    {
        animator.SetTrigger("Jump");
    }

    void Attack()
    {
        // Activa el trigger de ataque en el Animator para reproducir la animaci�n de ataque
        animator.SetTrigger("Attack");
    }

    void Defend()
    {
        // Activa el trigger de ataque en el Animator para reproducir la animaci�n de ataque
        animator.SetTrigger("Defend");
    }

    public void moveCharacter(float hInput, float vInput, Camera cam, bool jump, bool dash)
    {
        //Calculate the Input Magnitude
        Speed = new Vector2(hInput, vInput).normalized.sqrMagnitude;

        //Dash only if character has reached maxSpeed (animator parameter value) 
        if(Speed >= Speed - rotationThreshold && dash)
        {
            Speed = 1.5f;
        }

        //Physically move player

        if(Speed > rotationThreshold)
        {
            animator.SetFloat(motionParam, Speed, StartAnimTime, Time.deltaTime);
            Vector3 forward = cam.transform.forward;
            Vector3 right = cam.transform.right;

            forward.y = 0f;
            right.y = 0f;

            forward.Normalize();
            right.Normalize();

            //Rotate the character towards desired move direction based on player input and camera position

            desiredMoveDirection = forward * vInput + right * hInput; 

            if(Vector3.Angle(transform.forward, desiredMoveDirection) >= degreesToTurn)
            {
                turns180 = true;
            }
            else
            {
                turns180 = false;
                transform.rotation = Quaternion.Slerp(transform.rotation,
                                                  Quaternion.LookRotation(desiredMoveDirection),
                                                  rotationSpeed * Time.deltaTime);
            }

            animator.SetBool(turn180Param, turns180);
        }
        else if (Speed < rotationThreshold) //detener al personaje
        {
            animator.SetBool(mirrorIdleParam, mirrorIdle);
            //Stop the character
            animator.SetFloat(motionParam, Speed, StopAnimTime, Time.deltaTime);
        }

        // Salto
        if (jump && isGrounded && Speed > rotationThreshold)
        {
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
            animator.SetBool("isJumping", true); // Aseg�rate de tener este par�metro en tu Animator
        }
        else if (isGrounded)
        {
            animator.SetBool("isJumping", false);
        }

        // Aplicar gravedad al personaje
        playerVelocity.y += gravityValue * Time.deltaTime;
        characterController.Move(playerVelocity * Time.deltaTime);
    }

    private void OnAnimatorIK(int layerIndex)
    {
        if (Speed < rotationThreshold) return; //evitamos con el return que el c�digo se ejecute si no es necesario

        float distanceToLeftFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.LeftFoot));
        float distanceToRightFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.RightFoot));

        if(distanceToLeftFoot > distanceToRightFoot)
        {
            mirrorIdle = true;
        }
        else
        {
            mirrorIdle = false;
        }
    }
}
