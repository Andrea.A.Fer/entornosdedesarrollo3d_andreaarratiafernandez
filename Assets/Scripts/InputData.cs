using UnityEngine;
using System;

[Serializable]
public struct InputData
{
    //Basic Movement
    public float hMovement; 

    public float vMovement;

    //Mouse rotation
    public float verticalMouse;

    public float horizontalMouse;

    //Extra movement 
    public bool dash;

    public bool jump;

    public bool cinematica;

    public void getInput()
    {
        //basic movement 
        hMovement = Input.GetAxis("Horizontal");
        vMovement = Input.GetAxis("Vertical");

        //Mouse/Joystick rotation
        verticalMouse = Input.GetAxis("Mouse Y");
        horizontalMouse = Input.GetAxis(("Mouse X"));

        //Extra movement
        dash = Input.GetButton("Dash");
        jump = Input.GetButtonDown("Jump");

        cinematica = Input.GetKeyDown(KeyCode.C);
    }

    public void resetInput()
    {
        //basic movement 
        hMovement = vMovement = verticalMouse = horizontalMouse = 0;

        dash = jump = cinematica = false;
    }
}
