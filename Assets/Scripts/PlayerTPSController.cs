using UnityEngine;
using System;
using Unity.VisualScripting;

public class PlayerTPSController : MonoBehaviour
{
    public Camera cam;

    //public UnityEvent onInteractionInput;
    private InputData input;

    private CharacterAnimBasedMovement characterMovement; 

    public bool blockInput { get; set; }

    public bool onInteractionZone {  get; set; }

    public static event Action OnInteractionInput;
    void Start()
    {
        characterMovement = GetComponent<CharacterAnimBasedMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (blockInput)
        {
            input.resetInput();
        }
        else
        {
            //Get input from player
            input.getInput();
        }

        if(onInteractionZone && input.cinematica)
        {
            OnInteractionInput?.Invoke();
        }
        else
        {
            //Move the character
            characterMovement.moveCharacter(input.hMovement, input.vMovement, cam, input.jump, input.dash);
        }
    }   
}
